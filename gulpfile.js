var gulp = require('gulp');
var del = require('del');
var path = require('path');
var webpack = require('webpack-stream');
var webpackConfig = require('./webpack.config.js');

gulp.task('clean', function() {
    return del.sync([
        './dist/**/*'
    ]);
});

gulp.task('static', function() {
    gulp
        .src(['./src/static/**/*'])
        .pipe(gulp.dest('./dist'));
});

gulp.task('webpack', function() {
    gulp
        .src('./src/js/entry.js')
        .pipe(webpack(webpackConfig))
        .pipe(gulp.dest('./dist'));
});

gulp.task('build', ['clean', 'static', 'webpack']);

gulp.task('watch', function() {
    gulp.watch('./src/static/', ['static']);
    gulp.watch([
        './src/**/*.js',
        './src/**/*.css',
        './src/index.html'
    ], ['webpack']);
});

gulp.task('default', ['build']);