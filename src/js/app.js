import React from 'react';
import ReactDOM from 'react-dom';
import {Router, Route, hashHistory} from 'react-router';

import FrontPage from './pages/frontPage';
import FFAPage from './pages/ffaPage';

ReactDOM.render(
    <Router history={hashHistory}>
        <Route path="/" component={FrontPage}/>
        <Route path="/ffa" component={FFAPage}/>
    </Router>,
    document.getElementById('app')
);