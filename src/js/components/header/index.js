import React from 'react';
import ReactDOM from 'react-dom';
import Link from './link';

export default class Header extends React.Component {
    render() {
        return (
            <div class="HeadNavigation">
            <div class="row">
                <div class="small-12 column">
                    <ul class="vertical medium-horizontal menu" data-responsive-menu="drilldown medium-dropdown">
                        <li><Link to="/" class="title">Dracade</Link></li>
                        <li><Link to="/ffa">FFA</Link></li>
                        <li><Link to="/cagematch">Cagematch</Link></li>
                        <li><Link to="/all">Overall</Link></li>
                        <li><a href="https://dracade.buycraft.net/" class="special">Store</a></li>
                    </ul>
                </div>
            </div>
            </div>
        );
    }
};