import React from 'react'
import { Link as ReactLink } from 'react-router'

export default class Link extends React.Component {
  render() {
    return (
      <ReactLink {...this.props} activeClassName='active'/>
    );
  }
}