import React from 'react';
import ReactDOM from 'react-dom';

export default class BodyContainer extends React.Component {
    render() {
        return (
            <div class="row" class="body-container-component">
                <div class="small-12 large-offset-2 large-8 column">
                    {this.props.children}
                </div>
            </div>
        );
    }
};