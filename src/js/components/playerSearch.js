import React from 'react';
import ReactDOM from 'react-dom';

export default class PlayerSearch extends React.Component {
    render() {
        return (
            <div class="player-search-component">
                <div class="input-group">
                    <input type="text" class="input-group-field" placeholder={this.props.placeholder} style={{borderRadius: "5px 0 0 5px"}}/>
                    <a class="input-group-button button" style={{borderRadius: "0 5px 5px 0"}}>Search</a>
                </div>
            </div>
        );
    }
};