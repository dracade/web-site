"use strict";
import React from 'react';
import ReactDOM from 'react-dom';

const TextCell = ({rowIndex, data, col, ...props}) => (
  <Cell {...props}>
    {data[rowIndex][col]}
  </Cell>
);

export default class Leaderboard extends React.Component {  

    constructor(props) {
        super(props);
    }

    render() {
        var {data, meta, columns} = this.props;
        var columnElements = [];
        var rowElements = [];

        for (var i=0; i<columns.length; i++) {
            columnElements.push(<th key={i}>{columns[i]}</th>);
        }
        
        for (var i=0; i<data.length; i++) {
            var rowDataElements = [];
            for (var x=0; x<columns.length; x++) {
                rowDataElements.push(<td key={x}>{data[i][columns[x]]}</td>);
            }
            rowElements.push(
                <tr key={i}>
                    {rowDataElements}
                </tr>
            );
        }

        return (
            <table>
                <thead>
                    <tr>
                        {columnElements}
                    </tr>
                </thead>
                <tbody>
                    {rowElements}
                </tbody>
            </table>
        );
    }

}