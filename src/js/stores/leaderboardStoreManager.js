import { EventEmitter } from 'events';
var Dispatcher = require('../dispatcher/dispatcher');

import MinecraftUUIDStore from './minecraftUUIDStore';

var stores = {};

class LeaderboardStore extends EventEmitter {

    constructor(gameCategory, gameName) {
        super();
        // Assign default variables
        this.queryURL = 'minigames/' + gameCategory + '/' + gameName;
        this.gameCategory = gameCategory;
        this.gameName = gameName;
        this.data = {};
        this.meta = {};
        // this.isUpdating = false;

        MinecraftUUIDStore.on('USERNAME_FOR_UUID', (function(payload){
            for (var i = 0; i < this.data.length; i++) {
                var entry = this.data[i];
                if (entry.uniqueId == payload.uuid) {
                    entry.username = payload.username;
                    this.data[i] = entry;
                    this.emit('update');
                }
            }
        }).bind(this));

        // Register 'onDispatcherCall' to the dispatcher
        Dispatcher.register(this.onDispatcherCall.bind(this));
    }

    /**
     * Whenever the dispatcher dispatches fire this function
     */
    onDispatcherCall(payload) {
        if (payload.action == 'UPDATE_LEADERBOARD') {
            if (payload.data.gameCategory == this.gameCategory && payload.data.gameName == this.gameName) {
                // Update this store's leaderboard
                this.updateStore();
            }
        }
    }

    /**
     * Update this store by quering the rest-api
     */
    updateStore() {
        // Query the api 
        jQuery
            .getJSON(AppConfig.ApiURL + this.queryURL)
            .done((( response ) => {
                // Save data if query was succesful
                this.data = response.data;
                this.meta = response.meta;
                for (var i = 0; i < this.data.length; i++) {
                    var entry = this.data[i];
                    console.log(entry);
                    Dispatcher.dispatch({
                        action: "USERNAME_FOR_UUID",
                        data: entry.uniqueId
                    });
                }
                // Notify listeners that we've update information
                this.emit('update');
            }).bind(this));
    }
}

// Export a function dat creates or gets the store
export default {
    getStore(gameCategory, gameName) {
        var store = stores[gameCategory+':'+gameName];
        if (typeof store == typeof undefined
            || store == undefined) {
                stores[gameCategory+':'+gameName] = new LeaderboardStore(gameCategory, gameName);
            }
        return stores[gameCategory+':'+gameName];
    }
};