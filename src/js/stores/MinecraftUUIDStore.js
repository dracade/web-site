import { EventEmitter } from 'events';
var Dispatcher = require('../dispatcher/dispatcher');

class MinecraftUUIDStore extends EventEmitter {

    constructor() {
        super();
        this.UsernameUUIDPairs = {};

        // Register dispatcher
        Dispatcher.register(this.onDispatcherCall.bind(this));
    }

    onDispatcherCall(payload) {
        if (payload.action == "USERNAME_FOR_UUID") {
            this.getUsername(payload.data);
        }
    }

    getUsername(uuid) {
        var username = this.UsernameUUIDPairs[uuid];
        if (username == undefined ||
            typeof username == typeof undefined) {
                // Query the mojang API for the username
                $.getJSON('https://mcapi.ca/name/uuid/'+uuid)
                    .done((function( response ) {
                        this.UsernameUUIDPairs[uuid] = response.name;
                        this.emit('USERNAME_FOR_UUID', {
                            uuid,
                            username: response.name
                        });
                    }).bind(this));
            } else {
                // Send the username if it is already stored
                this.emit('USERNAME_FOR_UUID', {
                    uuid,
                    username: username
                });
            }
    }

    getUUID(username) {

    }
}

export default new MinecraftUUIDStore();