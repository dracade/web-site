import React from 'react';
import ReactDOM from 'react-dom';
import Dispatcher from '../dispatcher/dispatcher';

// FLUX Components
import LeaderboardStoreManager from '../stores/leaderboardStoreManager';
import MinecraftUUIDStore from '../stores/minecraftUUIDStore';

// VIEW Components
import Header from '../components/header';
import BodyContainer from '../components/bodyContainer';
import PlayerSearch from '../components/playerSearch';
import Leaderboard from '../components/leaderboard';

export default class FFAPage extends React.Component {

    constructor() {
        super();
        this.state = {
            leaderboardData: [],
            leaderboardMeta: {
                count: 0
            }
        };
        // Get or create store
        this.FFAStore = LeaderboardStoreManager.getStore('battleground', 'freeforall');
        // Set state to new data on 'update' event
        this.FFAStore.on('update', (function() {
            this.setState({
                leaderboardData: this.FFAStore.data,
                leaderboardMeta: this.FFAStore.meta
            });
        }).bind(this));

        MinecraftUUIDStore.on('USERNAME_FOR_UUID', (function(payload) {
            console.log(payload);
            this.state.leaderboardData.map((function(entry){
                if (entry.uniqueId == payload.uuid)
                    entry.username = payload.username;
            }).bind(this))
        }).bind(this));
    }

    componentDidMount() {
        // Query rest-api here.
        // http://localhost/api/minigames/battleground/freeforall
        this.updateLeaderboard();
    }

    updateLeaderboard() {
        Dispatcher.dispatch({
            action: "UPDATE_LEADERBOARD",
            data: {
                gameCategory: 'battleground',
                gameName: 'freeforall'
            }
        });
    }

    render() {
        var data = this.state.leaderboardData;
        var meta = this.state.leaderboardMeta;
        return (
            <div>
                <Header/>
                <BodyContainer>
                    <div class="row panel panel-inset" style={{backgroundColor: "rgba(10.5%, 13.3%, 14.8%, 0.7)", marginTop: "35px"}}>
                        <div class="small-12 large-6 large-offset-3 column">
                            <div style={{paddingTop: "55px", paddingBottom: "55px"}}><PlayerSearch placeholder="_Notch"/></div>
                        </div>
                    </div>
                    <div class="row panel panel-inset" style={{marginTop: "35px"}}>
                        <h2 class="text-center">FreeForAll Leaderboards</h2>
                        <a onClick={this.updateLeaderboard.bind(this)}>Update</a>
                        <div class="small-12 column">
                            <Leaderboard data={data} meta={meta} columns={['username','totalKills', 'totalDeaths', 'totalTimePlayed']} />
                        </div>
                    </div>
                </BodyContainer>
            </div>
        );
    }
};