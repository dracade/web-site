import React from 'react';
import ReactDOM from 'react-dom';

import Header from '../components/header';
import BodyContainer from '../components/bodyContainer';
import PlayerSearch from '../components/playerSearch';

export default class FrontPage extends React.Component {
    render() {
        return (
            <div>
                <Header/>
                <BodyContainer>
                    <div class="row panel panel-inset" style={{backgroundColor: "rgba(10.5%, 13.3%, 14.8%, 0.7)", marginTop: "35px"}}>
                        <div class="small-12 large-6 large-offset-3 column">
                            <div style={{paddingTop: "55px", paddingBottom: "55px"}}><PlayerSearch placeholder="_Notch"/></div>
                        </div>
                    </div>
                    <div class="row panel panel-inset" style={{marginTop: "35px"}}>
                        <h2 class="text-center">Dracade's Leaderboards</h2>
                    </div>
                </BodyContainer>
            </div>
        );
    }
};